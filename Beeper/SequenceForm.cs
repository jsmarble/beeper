﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Beeper.Properties;

namespace Beeper
{
    public partial class SequenceForm : Form
    {
        private BeepSequence sequence;
        private BindingList<Beep> beepsBindingList;

        public SequenceForm()
        {
            InitializeComponent();
            this.LoadBeepSequence(new BeepSequence());
        }

        private void lstBeeps_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Beep selectedBeep = lstBeeps.SelectedItem as Beep;
            if (selectedBeep != null)
                EditBeep(lstBeeps.SelectedItem as Beep);
        }

        private void toolAdd_Click(object sender, EventArgs e)
        {
            Beep beep = new Beep();
            if (EditBeep(beep) == DialogResult.OK)
            {
                sequence.Beeps.Add(beep);
                beepsBindingList.ResetBindings();
                lstBeeps.SelectedIndex = sequence.Beeps.Count - 1;
            }
        }

        private void toolDelete_Click(object sender, EventArgs e)
        {
            Beep selectedBeep = lstBeeps.SelectedItem as Beep;
            if (selectedBeep != null)
                sequence.Beeps.Remove(selectedBeep);
            beepsBindingList.ResetBindings();
        }

        private DialogResult EditBeep(Beep beep)
        {
            using (EditBeepForm frm = new EditBeepForm(beep))
                return frm.ShowDialog(this);
        }

        private void LoadBeepSequence(BeepSequence sequence)
        {
            if (sequence != null)
            {
                if (this.sequence != null)
                {
                    this.sequence.Stop();
                    this.sequence.PlaybackStatusChanged -= new EventHandler(sequence_PlaybackStatusChanged);
                }
                this.sequence = sequence;
                this.sequence.PlaybackStatusChanged += new EventHandler(sequence_PlaybackStatusChanged);
                beepsBindingList = new BindingList<Beep>(sequence.Beeps);
                this.beepBindingSource.DataSource = beepsBindingList;
            }
        }

        void sequence_PlaybackStatusChanged(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
                this.Invoke((MethodInvoker)this.SetPlaybackControlButtons);
            else
                this.SetPlaybackControlButtons();
        }

        private void toolMoveUp_Click(object sender, EventArgs e)
        {
            Beep selectedBeep = lstBeeps.SelectedItem as Beep;
            if (selectedBeep != null)
            {
                int i = sequence.Beeps.IndexOf(selectedBeep);
                if (i > 0)
                {
                    sequence.Beeps.RemoveAt(i);
                    sequence.Beeps.Insert(i - 1, selectedBeep);
                    lstBeeps.SelectedIndex = i - 1;
                    beepsBindingList.ResetBindings();
                }
            }
        }

        private void toolMoveDown_Click(object sender, EventArgs e)
        {
            Beep selectedBeep = lstBeeps.SelectedItem as Beep;
            if (selectedBeep != null)
            {
                int i = sequence.Beeps.IndexOf(selectedBeep);
                if (i < sequence.Beeps.Count - 1)
                {
                    sequence.Beeps.RemoveAt(i);
                    sequence.Beeps.Insert(i + 1, selectedBeep);
                    lstBeeps.SelectedIndex = i + 1;
                    beepsBindingList.ResetBindings();
                }
            }
        }

        private void SetPlaybackControlButtons()
        {
            if (sequence != null)
            {
                switch (sequence.PlaybackStatus)
                {
                    case PlaybackState.Playing:
                        toolPlay.Image = Resources.control_play;
                        toolStop.Image = Resources.control_stop_blue;
                        toolPause.Image = Resources.control_pause_blue;
                        break;
                    case PlaybackState.Paused:
                        toolPlay.Image = Resources.control_play;
                        toolStop.Image = Resources.control_stop_blue;
                        toolPause.Image = Resources.control_pause_blue;
                        break;
                    case PlaybackState.Stopped:
                        toolPlay.Image = Resources.control_play_blue;
                        toolStop.Image = Resources.control_stop;
                        toolPause.Image = Resources.control_pause;
                        break;
                    default:
                        break;
                }
            }
        }

        private void toolSave_Click(object sender, EventArgs e)
        {
            if (sequence != null)
                using (SaveFileDialog dlg = new SaveFileDialog())
                    if (dlg.ShowDialog(this) == DialogResult.OK)
                        sequence.Save(dlg.FileName);
        }

        private void toolOpen_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
                if (dlg.ShowDialog(this) == DialogResult.OK)
                    this.LoadBeepSequence(BeepSequence.FromFile(dlg.FileName));
        }

        private void toolPlay_Click(object sender, EventArgs e)
        {
            sequence.Play();
        }

        private void toolPause_Click(object sender, EventArgs e)
        {
            sequence.Pause();
        }

        private void toolStop_Click(object sender, EventArgs e)
        {
            sequence.Stop();
        }
    }
}
