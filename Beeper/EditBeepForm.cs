﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Beeper
{
    public partial class EditBeepForm : Form
    {
        public EditBeepForm(Beep beep)
        {
            if (beep == null)
                throw new ArgumentNullException("beep");

            InitializeComponent();
            numFrequency.Minimum = Beep.FREQUENCY_MIN;
            numFrequency.Maximum = Beep.FREQUENCY_MAX;
            beepBindingSource.DataSource = beep;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.beepBindingSource.CancelEdit();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.beepBindingSource.EndEdit();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
