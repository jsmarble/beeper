﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Beeper
{
    public class BeepXmlWriter
    {
        public void Write(BeepSequence value, Stream outputStream)
        {
            XmlSerializer ser = new XmlSerializer(typeof(BeepSequence));
            ser.Serialize(outputStream, value);
        }

        public void WriteFile(BeepSequence value, string path)
        {
            using (FileStream fs = File.OpenWrite(path))
                this.Write(value, fs);
        }
    }
}
