﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Beeper
{
    public class BeepXmlReader
    {
        public BeepSequence Read(Stream inputStream)
        {
            XmlSerializer ser = new XmlSerializer(typeof(BeepSequence));
            BeepSequence result = ser.Deserialize(inputStream) as BeepSequence;
            return result;
        }

        public BeepSequence ReadFile(string path)
        {
            using (FileStream fs = File.OpenRead(path))
                return this.Read(fs);
        }
    }
}
